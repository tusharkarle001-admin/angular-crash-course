import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Task } from '../../Tasks';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-tasks-item',
  templateUrl: './tasks-item.component.html',
  styleUrls: ['./tasks-item.component.css'],
})
export class TasksItemComponent implements OnInit {
  @Input() task: Task;
  faTimes = faTimes;
  constructor() {}
  @Output() onDeleteTask: EventEmitter<Task> = new EventEmitter();
  @Output() onToggleReminder: EventEmitter<Task> = new EventEmitter();
  ngOnInit(): void {}

  onDelete(giventask: any) {
    this.onDeleteTask.emit(giventask);
  }

  onToggle(task: Task) {
    this.onToggleReminder.emit(task);
  }
}
